
package fr.eftl.dev.api.springboot.crm.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderRepositoryImplTest {

	@Autowired
	private OrderRepository orderRepository;

	@Test
	void testFindAllOrders() {
		List<Order> orders = orderRepository.findAll();
		assertEquals(3, orders.size());
	}

//	@Test // A REVISER
//	void testFindOrderbyTypeAndStatus() {
//		Optional<List<Order>> orders = orderRepository.findByTypeAndStatus("Forfait", "En cours");
//		assertEquals(1, orders.get().size());
//	}

	@Test
	void testFindOrdersByCustomerId() {
		Customer customer = new Customer();
		customer.setId(2);
		List<Order> orders = orderRepository.findByCustomer(customer);
		assertEquals(1, orders == null ? 0 : orders.size());
	}

	@Test
	void testFindOrderbyStatus() {
		Optional<List<Order>> orders = orderRepository.findByStatus("En attente");
		assertEquals(1, orders.get().size());
	}

	@Test
	void testFindOrderbyType() {
		List<Order> orders = orderRepository.findByType("Forfait");
		assertEquals(3, orders.size());
	}

	@Test
	void testCreateOrder() {
		Order newOrder = new Order();
		newOrder.setId(3);
		newOrder.setAdrEt(236.0);
		newOrder.setLabel("Formation Spring Boot");
		newOrder.setNumberOfDays(10.0);
		newOrder.setTva(19.8);
		newOrder.setStatus("En attente");
		newOrder.setType("Forfait");
		newOrder.setNotes("Test en application de spring boot");

		orderRepository.save(newOrder);

		Optional<Order> order = orderRepository.findById(3);
		Assertions.assertNotNull(order, "order 3 not found");
	}

	@Test
	void testUpdateOrder() {

		Optional<Order> order = orderRepository.findById(2);
		if (order.isEmpty()) {
			fail();
		}
		order.get().setAdrEt(500.00);
		order.get().setStatus("Payée");

		orderRepository.save(order.get());

		Optional<Order> miseAJourOrder = orderRepository.findById(2);
		assertEquals(500.00, miseAJourOrder.get().getAdrEt());
	}

	@Test
	void testDeleteOrder() {
		Optional<Order> order = orderRepository.findById(2);
		if (order.isEmpty()) {
			fail();
		}
		orderRepository.delete(order.get());

		Optional<Order> deletedOrder = orderRepository.findById(2);
		Assertions.assertTrue(deletedOrder.isEmpty(), "Deleted order must be null");

	}

}