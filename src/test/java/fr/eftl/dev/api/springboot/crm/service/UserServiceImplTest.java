package fr.eftl.dev.api.springboot.crm.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.User;
import fr.eftl.dev.api.springboot.crm.repository.UserRepository;
import fr.eftl.dev.api.springboot.crm.service.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

	@InjectMocks
	private UserServiceImpl userService;
	@Mock
	private UserRepository userRepository;
	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testGetAllUsers() {
		List<User> users = new ArrayList<>();
		users.add(new User(99, "ANNE", "88888", "anne@mail.com", "ADMIN"));
		Mockito.when(userRepository.findAll()).thenReturn(users);

		List<User> usersRecup = userService.getAllUsers();
		assertEquals(1, usersRecup.size());
	}

	@Test
	void testGetUserByIdOK() {
		Optional<User> userTest = Optional.of(new User(99, "ANNE", "88888", "anne@mail.com", "ADMIN"));
		Mockito.when(userRepository.findById(99)).thenReturn(userTest);
		User user = userService.getUserById(99);
		assertEquals(99, user.getId());
	}

	@Test
	void testGetUserByIdKO() {
		Mockito.when(userRepository.findById(99)).thenThrow(UnknownResourceException.class);
		assertThrows(UnknownResourceException.class, () -> userService.getUserById(99));
	}

	@Test
	void testCreateUser() {
		User user = new User();
		user.setUsername("ANNE");
		Mockito.when(userRepository.save(user)).thenReturn(user);
		User createdUser = userService.createUser(user);
		assertNotNull(createdUser);
	}

	@Test
	void testUpdateUserOK() {
		User user = new User();
		user.setId(1);
		user.setGrants("ADMIN");
		Optional<User> usersTest = Optional.of(user);
		Mockito.when(userRepository.findById(1)).thenReturn(usersTest);
		Mockito.when(userRepository.save(user)).thenReturn(user);
		try {
			userService.updateUser(user, false);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testUpdateUserKO() {
		User user = new User();
		user.setId(1);
		user.setGrants("ADMIN");
		Optional<User> userTest = Optional.empty();
		Mockito.when(userRepository.findById(1)).thenReturn(userTest);
		assertThrows(UnknownResourceException.class, () -> userService.updateUser(user, false),
				"UnknownResourceException attendue !");
		Mockito.verify(userRepository, Mockito.times(0)).save(user);
	}

	@Test
	void testDeleteUserOK() {
		User user = new User();
		user.setId(3);
		Optional<User> userTest = Optional.of(user);
		Mockito.when(userRepository.findById(3)).thenReturn(userTest);
		Mockito.doNothing().when(userRepository).delete(user);
		try {
			userService.getUserById(3);
			userService.deleteUser(3);
		} catch (Exception e) {
			fail();
		}
		Mockito.verify(userRepository, Mockito.times(1)).delete(user);
	}

	@Test
	void testDeleteUserKO() {
		User user = new User();
		user.setId(3);
		Optional<User> userTest = Optional.empty();
		Mockito.when(userRepository.findById(3)).thenReturn(userTest);
		assertThrows(UnknownResourceException.class, () -> userService.deleteUser(3),
				"UnknownResourceException attendue !");

		Mockito.verify(userRepository, Mockito.times(0)).delete(user);
	}

	@Test
	void testGetUserByUsername() {
		User userTest = new User(99, "ANNE", "88888", "anne@mail.com", "ADMIN");
		Mockito.when(userRepository.findByUsername("ANNE")).thenReturn(userTest);
		User user = userService.getUserByUsername("ANNE");
		assertEquals(99, user.getId());
	}

	@Test
	void GetUserByUsernameAndPassword() {
		User userTest = new User(99, "ANNE", "$2y$10$P/KBecnLhTL8oMeUMX6FHeZebu5XW59Vw2qLYbLPKYcn1DEnzMFIy",
				"anne@mail.com", "ROLE_ADMIN");
		Mockito.when(userRepository.findByUsername("ANNE")).thenReturn(userTest);
		Mockito.when(
				bCryptPasswordEncoder.matches("88888", "$2y$10$P/KBecnLhTL8oMeUMX6FHeZebu5XW59Vw2qLYbLPKYcn1DEnzMFIy"))
				.thenReturn(true);
		User user = userService.getUserByUsernameAndPassword("ANNE", "88888");
		assertEquals("$2y$10$P/KBecnLhTL8oMeUMX6FHeZebu5XW59Vw2qLYbLPKYcn1DEnzMFIy", user.getPassword());
	}

}
