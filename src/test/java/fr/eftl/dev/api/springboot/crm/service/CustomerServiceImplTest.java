package fr.eftl.dev.api.springboot.crm.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.eftl.dev.api.springboot.crm.app.exception.CustomerWithOrderDeleteException;
import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.repository.CustomerRepository;
import fr.eftl.dev.api.springboot.crm.repository.OrderRepository;
import fr.eftl.dev.api.springboot.crm.service.impl.CustomerServiceImpl;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

	@InjectMocks
	private CustomerServiceImpl customerService;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private OrderRepository orderRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testGetAllCustomers() {
		List<Customer> customers = new ArrayList<>();
		customers.add(new Customer(99, "test", "test", "test", "test", "test", "test", "test", true));
		Mockito.when(customerRepository.findAll()).thenReturn(customers);

		List<Customer> customersRecup = customerService.getAllCustomers();
		assertEquals(1, customersRecup.size());
	}

	@Test
	void testGetCustomerByIdOK() {
		Optional<Customer> customerTest = Optional
				.of(new Customer(99, "test", "test", "test", "test", "test", "test", "test", true));
		Mockito.when(customerRepository.findById(99)).thenReturn(customerTest);
		Customer customer = customerService.getCustomerById(99);
		assertEquals(99, customer.getId());
	}

	@Test
	void testGetCustomerByIdKO() {
		Mockito.when(customerRepository.findById(99)).thenThrow(UnknownResourceException.class);
		assertThrows(UnknownResourceException.class, () -> customerService.getCustomerById(99));
	}

	@Test
	void testGetCustomerByActiveTrue() {
		List<Customer> mockCustomers = new ArrayList<Customer>();
		mockCustomers.add(new Customer(99, "test", "test", "test", "test", "test", "test", "test", true));
		Mockito.when(customerRepository.findByActive(true)).thenReturn(mockCustomers);
		List<Customer> customers = customerService.getCustomersByActive(true);
		assertEquals(1, customers.size());
	}

	@Test
	void testGetCustomerByActiveFalse() {
		List<Customer> mockCustomers = new ArrayList<Customer>();
		mockCustomers.add(new Customer(99, "test", "test", "test", "test", "test", "test", "test", false));
		Mockito.when(customerRepository.findByActive(false)).thenReturn(mockCustomers);
		List<Customer> customers = customerService.getCustomersByActive(false);
		assertEquals(1, customers.size());
	}

	@Test
	void testGetCustomerwithMobile() {
		List<Customer> mockCustomers = new ArrayList<Customer>();
		mockCustomers.add(new Customer(99, "test", "test", "test", "test", "test", "mobile", "test", true));
		Mockito.when(customerRepository.findCustomersWithMobile()).thenReturn(mockCustomers);
		List<Customer> customers = customerService.getCustomersWithMobile();
		assertEquals(1, customers.size());
	}

	@Test
	void testCreate() {
		Customer customer = new Customer();
		customer.setLastname("MARTIN");
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		Customer createdCustomer = customerService.createCustomer(customer);
		assertNotNull(createdCustomer);
	}

	@Test
	void testUpdate() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setFirstname("Bob");
		Optional<Customer> customerTest = Optional.of(customer);
		Mockito.when(customerRepository.findById(1)).thenReturn(customerTest);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		try {
			customerService.updateCustomer(customer);
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testUpdateKO() {
		Customer customer = new Customer();
		customer.setId(1);
		customer.setFirstname("Bob");
		Optional<Customer> customerTest = Optional.empty();

		Mockito.when(customerRepository.findById(1)).thenReturn(customerTest);
		assertThrows(UnknownResourceException.class, () -> customerService.updateCustomer(customer),
				"UnknownResourceException attendue !");
		Mockito.verify(customerRepository, Mockito.times(0)).save(customer);
	}

	@Test
	void testDelete() {
		Customer customer = new Customer();
		customer.setId(3);
		Optional<Customer> customerTest = Optional.of(customer);
		Order order = new Order();
		order.setId(3);
		order.setCustomer(customer);
		List<Order> custOrderList = new ArrayList<>();
		Mockito.when(customerRepository.findById(3)).thenReturn(customerTest);
		Mockito.when(orderRepository.findByCustomer(customer)).thenReturn(custOrderList);
		Mockito.doNothing().when(customerRepository).delete(customer);

		try {
			customerService.deleteCustomer(3);
		} catch (Exception e) {
			fail();
		}
		Mockito.verify(customerRepository, Mockito.times(1)).delete(customer);
	}

	@Test
	void testDeleteCustomerWithOrder() {
		Customer customer = new Customer();
		customer.setId(2);
		Optional<Customer> customerTest = Optional.of(customer);
		Order order = new Order();
		order.setId(3);
		order.setCustomer(customer);
		List<Order> custTestOrderList = new ArrayList<>();
		custTestOrderList.add(order);
		Mockito.when(orderRepository.findByCustomer(customer)).thenReturn(custTestOrderList);
		Mockito.when(customerRepository.findById(2)).thenReturn(customerTest);
		assertThrows(CustomerWithOrderDeleteException.class, () -> customerService.deleteCustomer(2),
				"Impossible de supprimer un client avec des commandes.");

	}

	@Test
	void testDeleteKO() {
		Customer customer = new Customer();
		customer.setId(3);
		Optional<Customer> customerTest = Optional.empty();

		Mockito.when(customerRepository.findById(3)).thenReturn(customerTest);
		assertThrows(UnknownResourceException.class, () -> customerService.deleteCustomer(3),
				"UnknownResourceException attendue !");
		Mockito.verify(customerRepository, Mockito.times(0)).delete(customer);
	}

}
