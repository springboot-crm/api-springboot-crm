package fr.eftl.dev.api.springboot.crm.rest.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

	public static final String URI = "/customers";
	public static final String FIRST_NAME = "John";
	public static final String LAST_NAME = "Doe";
	public static final String COMPANY = "Entreprise";
	public static final String MAIL = "mail@mail.com";
	public static final String PHONE = "0202020202";
	public static final String MOBILE = "0606060606";
	public static final String NOTES = "Les notes";
	public static final String ACTIVE = "true";

	@Autowired
	private MockMvc mockMvc;

	@Test
	@WithMockUser(roles = "ADMIN")
	void CustomersGetShouldReturnTailoredMessage() throws Exception {
		this.mockMvc.perform(get("/customers/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Indiana")));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void CustomersGetShouldReturnAllCustomers() throws Exception {
		this.mockMvc.perform(get("/customers")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Marty")));
	}
	
	@Test
	@WithMockUser(roles = "ADMIN")
	void CustomerGetShouldReturnActiveCustomers() throws Exception {
		this.mockMvc.perform(get("/customers/IsActive?active=true")).andDo(print()).andExpectAll(status().isOk(),
				content().string(containsString("KENOBI")));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void CustomerGetShouldReturnNonActiveCustomers() throws Exception {
		this.mockMvc.perform(get("/customers/IsActive?active=false")).andDo(print()).andExpectAll(status().isOk(),
				content().string(containsString("MCCLANE")));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenFirstnameMissingNameThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + LAST_NAME + "\",\r\n"
						+ "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE + "\",\r\n"
						+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \""
						+ NOTES + "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenLastnameMissingNameThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"firstname\": \"" + FIRST_NAME + "\",\r\n"
						+ "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE + "\",\r\n"
						+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \""
						+ NOTES + "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenFirstnameTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + LAST_NAME + "\",\r\n"
						+ "                \"firstname\": \"" + "a" + "\",\r\n" + "                \"company\": \""
						+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
						+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \""
						+ MOBILE + "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n"
						+ "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenLastnameTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + "a" + "\",\r\n"
						+ "                \"firstname\": \"" + FIRST_NAME + "\",\r\n"
						+ "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE + "\",\r\n"
						+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \""
						+ NOTES + "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenCompanyMissingThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + LAST_NAME + "\",\r\n"
						+ "                \"firstname\": \"" + FIRST_NAME + "\",\r\n" + "\",\r\n"
						+ "                \"mail\": \"" + MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE
						+ "\",\r\n" + "                \"mobile\": \"" + MOBILE + "\",\r\n"
						+ "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": " + ACTIVE
						+ "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	// @Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenPhoneTooShortThenFailure() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + LAST_NAME + "\",\r\n"
						+ "                \"firstname\": \"" + FIRST_NAME + "\",\r\n"
						+ "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\",\r\n" + "                \"phone\": \"030303\",\r\n"
						+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \""
						+ NOTES + "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	void checkCustomerInfoWhenValidRequestThenSuccess() throws Exception {
		MockHttpServletRequestBuilder createCustomer = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"lastname\": \"" + LAST_NAME + "\",\r\n"
						+ "                \"firstname\": \"" + FIRST_NAME + "\",\r\n"
						+ "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE + "\",\r\n"
						+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \""
						+ NOTES + "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}");

		mockMvc.perform(createCustomer).andExpect(status().is2xxSuccessful());
	}

}
