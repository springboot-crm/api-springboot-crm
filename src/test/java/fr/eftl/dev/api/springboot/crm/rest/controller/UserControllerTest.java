package fr.eftl.dev.api.springboot.crm.rest.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

	public static final String URI = "/users";
	public static final String USERNAME = "toto";
	public static final String PASSWORD = "1234";
	public static final String USERNAME2 = "batman";
	public static final String GRANTS = "ADMIN";
	public static final String MAIL = "toto@titi.com";

	@Autowired
	private MockMvc mockMvc;

	// test methode get user by id
	@Test
	@WithMockUser(roles = "ADMIN")
	void testGetUserById() throws Exception {

		this.mockMvc.perform(get("/users/1")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("toto")));
	}

	// test methode get user by username
	@Test
	@WithMockUser(roles = "ADMIN")
	void testGetUserByName() throws Exception {

		this.mockMvc.perform(get("/users").param("username", "toto")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("toto")));
	}

	// test methode get user by username et password
	@Test
	@WithMockUser(roles = "ADMIN")
	void testGetUserByNamePsw() throws Exception {

		this.mockMvc.perform(get("/users/login?username=toto&password=1234")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("toto")));
	}

//	// test methode get all users // A REVISER
//	@Test
//	@WithMockUser(roles = "ADMIN")
//	void testAllUsers() throws Exception {
//		// List<User> userList = new ArrayList();
//		String test = "[{\r\n" + "                \"username\": \"" + USERNAME + "\",\r\n"
//				+ "                \"password\": \"" + PASSWORD + "\",\r\n" 
//				+ "        \"grants\": \"" + GRANTS
//				+ "\",\r\n" + "                \"mail\": \"" + MAIL + "\"\r\n }]";
//
//		this.mockMvc.perform(get("/users")).andDo(print()).andExpect(status().isOk()).andExpect(content().json(test));
//	}

	// test methode create user si ok
	@Test
	@WithMockUser(roles = "ADMIN")
	void checkWhenCreateUserok() throws Exception {
		MockHttpServletRequestBuilder createUser = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"username\": \"" + USERNAME2 + "\",\r\n"
						+ "                \"password\": \"" + PASSWORD + "\",\r\n" + "        \"grants\": \"" + GRANTS
						+ "\",\r\n" + "                \"mail\": \"" + MAIL + "\"\r\n}");
		mockMvc.perform(createUser).andExpect(status().is2xxSuccessful());
	}

	// test methode create user is pwd manquant
	@Test
	@WithMockUser(roles = "ADMIN")
	void CreateUserPasok() throws Exception {
		MockHttpServletRequestBuilder createUser = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"username\": \"" + USERNAME + "\",\r\n"
						+ "                \"password\": \"" + "        \"grants\": \"" + GRANTS + "\",\r\n"
						+ "                \"mail\": \"" + MAIL + "\"\r\n }");
		mockMvc.perform(createUser).andExpect(status().is4xxClientError());
	}

	// test methode create user is grants manquant
	@Test
	@WithMockUser(roles = "ADMIN")
	void CreateUserPasdeGrants() throws Exception {
		MockHttpServletRequestBuilder createUser = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"username\": \"" + USERNAME + "\",\r\n"
						+ "                \"password\": \"" + PASSWORD + "\",\r\n" + "                \"mail\": \""
						+ MAIL + "\"\r\n }");
		mockMvc.perform(createUser).andExpect(status().is4xxClientError());
	}

	// test methode delete user
	@Test
	@WithMockUser(roles = "ADMIN")
	void checkWhenDeleteUserok() throws Exception {
		this.mockMvc.perform(delete("/users/1")).andExpect(status().is2xxSuccessful());
	}

	// test methode update user
	@Test
	@WithMockUser(roles = "ADMIN")
	void checkWhenUpdateUserok() throws Exception {
		MockHttpServletRequestBuilder updateUser = put(URI+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"username\": \"" + USERNAME + "\",\r\n"
						+ "                \"password\": \"" + PASSWORD + "\",\r\n" + "        \"grants\": \"" + GRANTS
						+ "\",\r\n" + "                \"mail\": \"" + MAIL + "\"\r\n }}");
		mockMvc.perform(updateUser).andExpect(status().is2xxSuccessful());
	}

	// test methode update user si username manquant
	@Test
	@WithMockUser(roles = "ADMIN")
	void UpdateUserPasOkUsername() throws Exception {
		MockHttpServletRequestBuilder updateUser = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"password\": \"" + PASSWORD + "\",\r\n" + "        \"grants\": \""
						+ GRANTS + "\",\r\n" + "                \"mail\": \"" + MAIL + "\"\r\n }}");
		mockMvc.perform(updateUser).andExpect(status().is4xxClientError());
	}

	// test methode update user si pwd manquant
	@Test
	@WithMockUser(roles = "ADMIN")
	void UpdateUserPasOkPwd() throws Exception {
		MockHttpServletRequestBuilder updateUser = put(URI+"/1").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\r\n" + "                \"username\": \"" + USERNAME + "\",\r\n" + "        \"grants\": \""
						+ GRANTS + "\",\r\n" + "                \"mail\": \"" + MAIL + "\"\r\n }}");
		mockMvc.perform(updateUser).andExpect(status().is4xxClientError());
	}

}
