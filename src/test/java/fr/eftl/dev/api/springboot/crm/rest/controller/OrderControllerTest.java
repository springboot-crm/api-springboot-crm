package fr.eftl.dev.api.springboot.crm.rest.controller;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {
	public static final String URI = "/orders";
	public static final String LABEL = "Formation Spring";
	public static final Double ADRET = 450.0;
	public static final Double NUMOFDAYS = 3.0;
	public static final Double TVA = 20.0;
	public static final String STATUS = "En attente";
	public static final String TYPE = "Forfait";
	public static final String NOTES = "Order Notes";
	public static final String CUSTID = "1";
	public static final String FIRST_NAME = "John";
	public static final String LAST_NAME = "Doe";
	public static final String COMPANY = "Entreprise";
	public static final String MAIL = "mail@mail.com";
	public static final String PHONE = "0202020202";
	public static final String MOBILE = "0606060606";
	public static final String CUSTNOTES = "Customer notes";
	public static final String ACTIVE = "true";
	@Autowired
	private MockMvc mockMvc;

	@Test
	@Order(1)
	@WithMockUser(roles = "ADMIN")
	void getordersByIdtShouldReturnTailoreddMessage() throws Exception {
		this.mockMvc.perform(get(URI + "/2")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(LABEL)));

	}

	@Test
	@Order(2)
	@WithMockUser(roles = "ADMIN")
	void getAllOrderShouldReturnTailoredMessage() throws Exception {
		this.mockMvc.perform(get(URI)).andExpect(jsonPath("$.[*]", hasSize(3)));
	}

	@Test
	@Order(3)
	@WithMockUser(roles = "ADMIN")
	void getOrderByTypeStatusShouldReturnTailoredMessage() throws Exception {
		this.mockMvc.perform(get(URI + "?type=Forfait&status=En attente"))
				.andExpect(jsonPath("$.[0].status").value(STATUS));
	}

	@Test
	@Order(4)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenAdrEtMissingThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n" + "                \"tva\": "
				+ TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n" + "                \"type\": \""
				+ TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n"
				+ "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(5)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenNoOfDaysMissingThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + ",\r\n" + "                \"tva\": " + TVA
				+ ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n" + "                \"type\": \""
				+ TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n"
				+ "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(6)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenTVAMissingThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(7)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenStatusMissingThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(8)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenTypeMissingThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n"
				+ "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Order(9)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenNotesMissingThenSucess() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + "\",\r\n"
				+ "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + CUSTNOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(10)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenLabelLessThanMinLengthThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + "AB" + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(11)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenStatusExcessMaxLengthThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \""
				+ "1234567890123456789012" + "\",\r\n" + "                \"type\": \"" + TYPE + "\",\r\n"
				+ "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"customerDto\": {\r\n"
				+ "                \"id\": " + CUSTID + ",\r\n" + "                \"lastname\": \"" + LAST_NAME
				+ "\",\r\n" + "                \"company\": \"" + COMPANY + "\",\r\n" + "                \"mail\": \""
				+ MAIL + "\",\r\n" + "                \"phone\": \"" + PHONE + "\",\r\n"
				+ "                \"mobile\": \"" + MOBILE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"active\": " + ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(12)
	@WithMockUser(roles = "ADMIN")
	void createOrderWhenTVAWrongFormatThenFailure() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + "S" + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(13)
	@WithMockUser(roles = "ADMIN")
	void createOrderCheckOrderInfoWhenValidRequestThenSuccess() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";

		MockHttpServletRequestBuilder createOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(createOrder).andExpect(status().is2xxSuccessful());
	}

	@Test
	@Order(14)
	@WithMockUser(roles = "ADMIN")
	void deleteOrderIdNotFoundThenFailure() throws Exception {
		this.mockMvc.perform(delete(URI + "/009")).andExpect(status().is4xxClientError());
	}

	@Test
	@Order(15)
	@WithMockUser(roles = "ADMIN")
	void deleteOrderIdFoundThenSuceess() throws Exception {
		this.mockMvc.perform(delete(URI + "/2")).andExpect(status().is2xxSuccessful());
	}

	@Test
	@Order(16)
	@WithMockUser(roles = "ADMIN")
	void ordersGetByCustomerShouldReturnTailoredMessage() throws Exception {
		this.mockMvc.perform(get(URI + "/customers/1")).andExpect(jsonPath("$.[*]", hasSize(2)));
	}

	@Test
	@Order(17)
	@WithMockUser(roles = "ADMIN")
	void updateOrderCheckOrderInfoWhenValidRequestThenSuccess() throws Exception {
		String jsonOrder = "{\r\n" + "                \"label\": \"" + LABEL + "\",\r\n" + "                \"adrEt\": "
				+ ADRET + ",\r\n" + "                \"numberOfDays\": " + NUMOFDAYS + ",\r\n"
				+ "                \"tva\": " + TVA + ",\r\n" + "                \"status\": \"" + STATUS + "\",\r\n"
				+ "                \"type\": \"" + TYPE + "\",\r\n" + "                \"notes\": \"" + NOTES
				+ "\",\r\n" + "                \"customerDto\": {\r\n" + "                \"id\": " + CUSTID + ",\r\n"
				+ "                \"lastname\": \"" + LAST_NAME + "\",\r\n" + "                \"company\": \""
				+ COMPANY + "\",\r\n" + "                \"mail\": \"" + MAIL + "\",\r\n"
				+ "                \"phone\": \"" + PHONE + "\",\r\n" + "                \"mobile\": \"" + MOBILE
				+ "\",\r\n" + "                \"notes\": \"" + NOTES + "\",\r\n" + "                \"active\": "
				+ ACTIVE + "\r\n" + "}}";
		MockHttpServletRequestBuilder updateOrder = post(URI).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(jsonOrder);
		this.mockMvc.perform(updateOrder).andExpect(status().is2xxSuccessful());

	}

	@Test
	@Order(18)
	@WithMockUser(roles = "ADMIN")
	void patchOrderCheckOrderInfoWhenValidRequestThenSuccess() throws Exception {
		this.mockMvc.perform(patch(URI + "/1?status=Payée")).andExpect(status().is2xxSuccessful());
	}
}
