package fr.eftl.dev.api.springboot.crm.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.repository.OrderRepository;
import fr.eftl.dev.api.springboot.crm.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	}

	@Override
	public Order getOrderById(Integer id) {
		return orderRepository.findById(id).orElseThrow(() -> new UnknownResourceException("UnknownResourceException"));
	}

	@Override
	public Order createOrder(Order order) {
		return orderRepository.save(order);
	}

	@Override
	public Order updateOrder(Order order) throws Exception {
		logger.debug("attempting to update order {}...", order.getId());
		Order orderExistant = orderRepository.findById(order.getId()).orElseThrow(UnknownResourceException::new);
		orderExistant.setLabel(order.getLabel());
		orderExistant.setTva(order.getTva());
		orderExistant.setNumberOfDays(order.getNumberOfDays());
		orderExistant.setAdrEt(order.getAdrEt());
		orderExistant.setType(order.getType());
		orderExistant.setStatus(order.getStatus());
		orderExistant.setNotes(order.getNotes());
		return orderRepository.save(order);
	}

	@Override
	public void deleteOrder(Integer id) {
		Order order = getOrderById(id);
		if (null != order) {
			orderRepository.delete(order);
		} else {
			logger.error("La commande n'existe pas : {}", id);
		}
	}

	@Override
	public void patchOrderStatus(Integer orderId, String status) throws Exception {
		status = status == null ? "" : status.replaceAll("[\n\r\t]", "_");
		Order order = getOrderById(orderId);
		logger.debug("attempting to patch Order {} with status " + order.getStatus() + " to status {}...", orderId,
				status);
		order.setStatus(status);
		orderRepository.save(order);

	}

	@Override
	public List<Order> getOrderByTypeAndStatus(String type, String status) {
		Optional<List<Order>> optionalOrdersList = orderRepository.findByTypeAndStatus(type, status);
		return optionalOrdersList.isEmpty() ? null : optionalOrdersList.get();
	}

	@Override
	public List<Order> getOrderByCustomer(Customer customer) throws Exception {
		return orderRepository.findByCustomer(customer);

	}

	@Override
	public List<Order> getOrderByStatus(String status) {
		Optional<List<Order>> optionalOrdersList = orderRepository.findByStatus(status);
		return optionalOrdersList.isEmpty() ? null : optionalOrdersList.get();
	}

}
