package fr.eftl.dev.api.springboot.crm.rest.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.eftl.dev.api.springboot.crm.dto.UserDto;
import fr.eftl.dev.api.springboot.crm.mapper.UserMapper;
import fr.eftl.dev.api.springboot.crm.model.User;
import fr.eftl.dev.api.springboot.crm.service.UserService;

@Validated
@RestController
@RequestMapping("/users")
@CrossOrigin(value = { "*" }, allowedHeaders = { "*" })
public class UserRestController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserMapper userMapper;

	@GetMapping("/{id}")
	public ResponseEntity<UserDto> getUserById(@NotNull @PathVariable(value = "id") Integer id) {
		User user = userService.getUserById(id);
		if (null == user) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return ResponseEntity.ok().body(userMapper.mapUserToUserDto(user));
	}

	@GetMapping("/login")
	public ResponseEntity<UserDto> getUserByUsernameAndPassword(
			@NotNull @Size(min = 2, max = 30) @RequestParam String username,
			@NotNull @Size(min = 2, max = 255) @RequestParam String password) {
		User user = userService.getUserByUsernameAndPassword(username, password);
		if (null == user) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok().body(userMapper.mapUserToUserDto(user));
	}

	// combine getAllUser and getUserByName in one method
	@GetMapping()
	public ResponseEntity<List<UserDto>> getAllUsers(
			@Size(min = 2, max = 30) @RequestParam(value = "username", required = false) String username) {

		List<UserDto> usersDto = new ArrayList<>();
		// getUserByName
		if (null != username) {
			User user = userService.getUserByUsername(username);
			if (null == user) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			usersDto.add(userMapper.mapUserToUserDto(user));
		} else { // getAllUser
			List<User> users = userService.getAllUsers();
			for (User user : users) {
				usersDto.add(userMapper.mapUserToUserDto(user));
			}
		}
		return ResponseEntity.ok().body(usersDto);
	}

	@PostMapping()
	public ResponseEntity<UserDto> createUser(@Valid @RequestBody final UserDto userDto) {
		User user = userMapper.mapUserDtoToUser(userDto);
		User newUser = userService.createUser(user);
		return ResponseEntity.created(URI.create("/users/" + newUser.getId()))
				.body(userMapper.mapUserToUserDto(newUser));

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteUser(@NotNull @PathVariable(value = "id") Integer id) {
		userService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> updateUser(@NotNull @PathVariable(value = "id") Integer id,
			@RequestBody @Valid final UserDto userDto) {
		User userToBeUpdated = userMapper.mapUserDtoToUser(userDto);
		userToBeUpdated.setId(id);
		userService.updateUser(userToBeUpdated, false);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<Void> updateUserPassword(@NotNull @PathVariable(value = "id") Integer id,
			@Size(min = 2, max = 255) @RequestParam(value = "password", required = true) String password) {
		User userToBeUpdated = userService.getUserById(id);
		userToBeUpdated.setPassword(password);
		userService.updateUser(userToBeUpdated, true);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
