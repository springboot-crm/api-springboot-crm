package fr.eftl.dev.api.springboot.crm.app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class CustomerWithOrderDeleteException extends RuntimeException {

	public CustomerWithOrderDeleteException() {
		super("Impossible de supprimer un client avec des commandes.");
	}

	public CustomerWithOrderDeleteException(String message) {
		super(message);
	}

	public CustomerWithOrderDeleteException(Throwable cause) {
		super(cause);
	}
}
