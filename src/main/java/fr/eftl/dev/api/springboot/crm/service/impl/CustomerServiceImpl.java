package fr.eftl.dev.api.springboot.crm.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eftl.dev.api.springboot.crm.app.exception.CustomerWithOrderDeleteException;
import fr.eftl.dev.api.springboot.crm.app.exception.UnknownResourceException;
import fr.eftl.dev.api.springboot.crm.model.Customer;
import fr.eftl.dev.api.springboot.crm.model.Order;
import fr.eftl.dev.api.springboot.crm.repository.CustomerRepository;
import fr.eftl.dev.api.springboot.crm.repository.OrderRepository;
import fr.eftl.dev.api.springboot.crm.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	public static final String ERROR_MSG_NO_CUSTOMER = "Aucun client trouvé!";

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OrderRepository orderRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getAllCustomers() {
		return customerRepository.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer getCustomerById(Integer customerId) {
		return customerRepository.findById(customerId)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_CUSTOMER));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getCustomersByActive(Boolean active) {
		return customerRepository.findByActive(active);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> getCustomersWithMobile() {
		return customerRepository.findCustomersWithMobile();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer createCustomer(Customer customer) {
		return customerRepository.save(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer updateCustomer(Customer customer) {
		logger.debug("attempting to update customer {}...", customer.getId());
		Customer clientExistant = customerRepository.findById(customer.getId())
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_CUSTOMER));
		clientExistant.setLastname(customer.getLastname());
		clientExistant.setFirstname(customer.getFirstname());
		clientExistant.setMail(customer.getMail());
		clientExistant.setCompany(customer.getCompany());
		clientExistant.setMobile(customer.getMobile());
		clientExistant.setNotes(customer.getNotes());
		clientExistant.setPhone(customer.getPhone());
		clientExistant.setActive(customer.isActive());
		return customerRepository.save(clientExistant);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCustomer(Integer customerId) {
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_CUSTOMER));
		// validate if customer has order before delete
		List<Order> custOrderExist = orderRepository.findByCustomer(customer);
		if (null != custOrderExist && !custOrderExist.isEmpty()) {
			throw new CustomerWithOrderDeleteException();
		}
		customerRepository.delete(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void patchCustomerStatus(Integer customerId, boolean active) throws Exception {
		logger.debug("attempting to patch customer {} with active = {}...", customerId, active);
		Customer existingCustomer = customerRepository.findById(customerId)
				.orElseThrow(() -> new UnknownResourceException(ERROR_MSG_NO_CUSTOMER));
		existingCustomer.setActive(active);
		customerRepository.save(existingCustomer);
	}

}
