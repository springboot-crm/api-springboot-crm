package fr.eftl.dev.api.springboot.crm.mapper;

import org.springframework.stereotype.Component;

import fr.eftl.dev.api.springboot.crm.dto.UserDto;
import fr.eftl.dev.api.springboot.crm.model.User;

@Component
public class UserMapper {

	public UserDto mapUserToUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setUsername(user.getUsername());
		userDto.setMail(user.getMail());
		String[] grants = user.getGrants().split("_");
		userDto.setGrants(grants.length == 2 ? grants[1] : null);
		return userDto;
	}

	public User mapUserDtoToUser(UserDto userDto) {
		User user = new User();
		user.setId(userDto.getId());
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());
		user.setMail(userDto.getMail());
		user.setGrants("ROLE_" + userDto.getGrants());
		return user;
	}

}
