package fr.eftl.dev.api.springboot.crm.service;

import java.util.List;

import fr.eftl.dev.api.springboot.crm.model.User;

public interface UserService {

	/**
	 * Get all users
	 * 
	 * @return a list of users
	 */
	List<User> getAllUsers();

	/**
	 * Get a user by id
	 * 
	 * @param id the id
	 * @return the user
	 */
	User getUserById(Integer id);

	/**
	 * Create a user
	 * 
	 * @param user the user to create
	 * @return the user
	 */
	User createUser(User user);

	/**
	 * Update a user
	 * 
	 * @param user           the user to update
	 * @param changePassword indicate to update password
	 * @return the user
	 */
	User updateUser(User user, boolean changePassword);

	/**
	 * Delete a user
	 * 
	 * @param id the id of the user
	 */
	void deleteUser(Integer id);

	User getUserByUsername(String username);

	User getUserByUsernameAndPassword(String username, String password);

}
