package fr.eftl.dev.api.springboot.crm.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.lang.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.eftl.dev.api.springboot.crm.model.Order;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDto {

	private Integer id;

	@NotNull
	@Size(min = 2, max = 100)
	private String firstname;

	@NotNull
	@Size(min = 2, max = 100)
	private String lastname;

	@NotNull
	@Size(min = 2, max = 200)
	private String company;

	@NotNull
	@Email
	@Size(min = 3, max = 255)
	private String mail;

	@NotNull(message = "phone est obligatoire")
	@Size(min = 10, max = 15)
	private String phone;

	@Nullable
	@Size(min = 10, max = 15)
	private String mobile;

	@Size(max = 255)
	private String notes;

	@NotNull
	private Boolean active;

	@Nullable
	private List<Order> orders;

	public CustomerDto() {
	}

	public CustomerDto(Integer id, String lastname, String firstname, String company, String mail, String phone,
			String mobile, String notes, Boolean active) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.company = company;
		this.mail = mail;
		this.phone = phone;
		this.mobile = mobile;
		this.notes = notes;
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
