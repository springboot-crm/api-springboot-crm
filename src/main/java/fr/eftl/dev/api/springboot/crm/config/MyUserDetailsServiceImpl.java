package fr.eftl.dev.api.springboot.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import fr.eftl.dev.api.springboot.crm.model.User;
import fr.eftl.dev.api.springboot.crm.repository.UserRepository;

public class MyUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);

		if (null == user) {
			throw new UsernameNotFoundException("User with username " + username + " not found !");
		}

		return new MyUserDetails(user);
	}

}
