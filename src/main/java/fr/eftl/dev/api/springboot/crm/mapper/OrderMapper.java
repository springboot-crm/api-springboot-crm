package fr.eftl.dev.api.springboot.crm.mapper;

import org.springframework.stereotype.Component;

import fr.eftl.dev.api.springboot.crm.dto.OrderDto;
import fr.eftl.dev.api.springboot.crm.model.Order;

@Component
public class OrderMapper {

	public OrderDto mapOrderToOrderDto(Order order) {
		OrderDto orderDto = new OrderDto();
		orderDto.setId(order.getId());
		orderDto.setLabel(order.getLabel());
		orderDto.setAdrEt(order.getAdrEt());
		orderDto.setNumberOfDays(order.getNumberOfDays());
		orderDto.setTva(order.getTva());
		orderDto.setStatus(order.getStatus());
		orderDto.setType(order.getType());
		orderDto.setNotes(order.getNotes());
		CustomerMapper mapper = new CustomerMapper();
		orderDto.setCustomerDto(mapper.mapCustomerToCustomerDto(order.getCustomer()));
		return orderDto;
	}

	public Order mapOrderDtoToOrder(OrderDto orderDto) {
		Order order = new Order();
		order.setId(orderDto.getId());
		order.setLabel(orderDto.getLabel());
		order.setAdrEt(orderDto.getAdrEt());
		order.setNumberOfDays(orderDto.getNumberOfDays());
		order.setTva(orderDto.getTva());
		order.setStatus(orderDto.getStatus());
		order.setType(orderDto.getType());
		order.setNotes(orderDto.getNotes());
		CustomerMapper mapper = new CustomerMapper();
		order.setCustomer(mapper.mapCustomerDtoToCustomer(orderDto.getCustomerDto()));

		return order;
	}

}
